import 'package:elearning/route/navigation_services.dart';
import 'package:elearning/route/routes.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  final String title = 'Home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: Container(),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Vocab'),
          onPressed: () {
            final topicId = '4735054791049216';
            NavigationService().pushNamed(ROUTER_STUDY, arguments: {"topicId": topicId});
          },
        ),
      ),
    );
  }
}