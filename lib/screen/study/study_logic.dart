import 'package:elearning/provider/study_game_model.dart';
import 'package:elearning/screen/study/study_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudyLogic {
  final String topicId;
  StudyGameModel studyGameModel;
  BuildContext context;

  StudyLogic({@required this.context, @required this.topicId}) {
    studyGameModel = context.read<StudyGameModel>();
  }

  loadData() async {
    await studyGameModel.loadData(topicId: topicId);
  }

  Future onAnswer<T>(AnswerType type, [T params]) async {
    await studyGameModel.onAnswer(type, params);
  }

  onContinue() {
    studyGameModel.onContinue();
  }

}