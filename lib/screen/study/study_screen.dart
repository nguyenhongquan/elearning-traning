import 'package:elearning/model/core/question.dart';
import 'package:elearning/model/game/game_object.dart';
import 'package:elearning/provider/study_game_model.dart';
import 'package:elearning/screen/study/game_item_view.dart';
import 'package:elearning/screen/study/study_logic.dart';
import 'package:elearning/screen/study/study_progress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum AnswerType { quiz, spelling, matching, flash }
enum StudyType { study, practice }

class StudyScreen extends StatefulWidget {
  final String topicId;

  StudyScreen(this.topicId);

  @override
  _StudyScreenState createState() => _StudyScreenState();
}

class _StudyScreenState extends State<StudyScreen>
    with TickerProviderStateMixin {
  String get topicId => widget.topicId;

  StudyLogic studyLogic;

  //animation
  AnimationController _controller;
  Animation<Offset> _animation;
  Animation<Offset> _animation2;

  @override
  void initState() {
    studyLogic = StudyLogic(context: context, topicId: topicId);
    studyLogic.loadData();

    //init animation
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    _animation = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero)
        .animate(_controller);
    _animation2 = Tween<Offset>(begin: Offset.zero, end: Offset(-1.0, 0.0))
        .animate(_controller);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tít le'),
      ),
      body: SafeArea(
        child: Consumer(
          builder: (_, StudyGameModel gameModel, __) {
            if (gameModel.isFinishGame) {
              return Center(
                child: Text('Finishedddd!'),
              );
            }

            GameObject previousGame = gameModel.previousGame;
            gameModel.previousGame = gameModel.currentGames;

            return Column(
              children: <Widget>[
                _buildStudyProgress(),
                Flexible(
                  child: _renderCurrentGame(
                    gameModel.currentGames,
                    previousGame,
                  ),
                ),
                _renderContinueBtn(),
              ],
            );
          },
        ),
      ),
    );
  }

  _buildStudyProgress() {
    var progress = context.read<StudyGameModel>().gameProgress;
    if (progress == null || progress.total == 0) return SizedBox();
    return StudyProgressWidget(
      correct: progress.correct,
      total: progress.total,
    );
  }

  Widget _renderCurrentGame(
    GameObject _current,
    GameObject _pre,
  ) {
    // if (_pre != _current) {
    //   startAnimation();
    // }
    //
    // return Expanded(
    //   child: Stack(
    //     children: [
    //       SlideTransition(
    //         position: _animation2,
    //         child: Container(),
    //       ),
    //       SlideTransition(
    //         position: _animation,
    //         child: GameItemView(
    //           gameObject: _current,
    //           onAnswer: studyLogic.onAnswer,
    //         ),
    //       ),
    //     ],
    //   ),
    // );

    return GameItemView(
      gameObject: _current,
      onAnswer: studyLogic.onAnswer,
    );
  }

  void startAnimation() {
    _controller.reset();
    _controller.forward();
  }


  Container _renderContinueBtn() {
    GameObject currentGame = context.read<StudyGameModel>().currentGames;
    if (currentGame == null)
      return Container(
        child: null,
      );
    Color _color = Theme.of(context).backgroundColor;
    if (currentGame.status == GameObjectStatus.waiting) {
      return Container(child: null);
    }
    if (currentGame.questionStatus == QuestionStatus.answeredCorrect) {
      _color = Colors.blueAccent;
      if (context.read<StudyGameModel>().listGames.isEmpty) {}
    } else if (currentGame.questionStatus == QuestionStatus.answeredIncorrect) {
      _color = Colors.red;
    }
    return Container(
      child: MaterialButton(
        onPressed: studyLogic.onContinue,
        clipBehavior: Clip.antiAlias,
        animationDuration: Duration(milliseconds: 200),
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: _color,
                border: Border.all(
                  color: _color,
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Center(
                child: Text(
                  (currentGame.questionStatus == QuestionStatus.answeredCorrect &&
                      context.read<StudyGameModel>().listGames.isEmpty)
                      ? 'Finish'
                      : 'Continue',
                  style: TextStyle(color: Colors.white),
                ))),
      ),
    );
  }
}
