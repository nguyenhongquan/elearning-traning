
import 'package:flutter/cupertino.dart';

requestApi<T, R>({@required Function call, Function(T input) transform, @required R defaultValue}) async {
  try {
    final result = await call();
    print('Phungtd - requestApi - result type: ${result.runtimeType}');
    if (transform != null) {
      return transform(result);
    }
    return result as R;
  } catch (e) {
    print('Phungtd: Call ${call.toString()}  failed - ${e.toString()}');
    return defaultValue;
  }
}