import 'package:elearning/provider/app_providers.dart';
import 'package:elearning/repository/sql_repository.dart';
import 'package:elearning/route/navigation_services.dart';
import 'package:elearning/route/route_router.dart';
import 'package:elearning/route/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  SqfliteRepository sqlRepo = new SqfliteRepository();
  await sqlRepo.initDb();
  AppProvider().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final NavigationService navigationService = new NavigationService();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: AppProvider().provides,
      child: MaterialApp(
        navigatorKey: navigationService.navigationKey,
        initialRoute: ROUTER_HOME,
        onGenerateRoute: generateRoute,
      ),
    );
  }
}

