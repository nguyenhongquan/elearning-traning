import 'package:elearning/route/routes.dart';
import 'package:elearning/screen/home/home_screen.dart';
import 'package:elearning/screen/study/study_screen.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  Map<String, dynamic> arguments = settings.arguments;
  switch (settings.name) {
    case ROUTER_HOME:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: HomeScreen(),
      );
    case ROUTER_STUDY:
      String topicId = arguments['topicId'];
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: StudyScreen(topicId),
      );
    default: return MaterialPageRoute(
      builder: (_) => Scaffold(
        body: Center(
          child: Text('No route defined for ${settings.name}')),
      ),
    );
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow
  );
}