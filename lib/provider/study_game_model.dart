import 'package:elearning/model/core/choice.dart';
import 'package:elearning/model/core/question.dart';
import 'package:elearning/model/game/game_object.dart';
import 'package:elearning/model/game/progress.dart';
import 'package:elearning/model/game/quiz_game_object.dart';
import 'package:elearning/provider/game_model.dart';
import 'package:elearning/screen/study/quiz_view.dart';
import 'package:elearning/screen/study/study_screen.dart';
import 'package:flutter/material.dart';

class StudyGameModel extends GameModel implements GamePlay {
  List<Question> questions = [];
  String currentTopic;
  GameObject previousGame;
  List<GameObject> listDone = [];

  loadData({@required String topicId}) async {
    resetListGame();
    questions.clear();
    this.currentTopic = topicId;
    List<Question> quesDb = [];

    print('Phungtd: StudyGameModel- load data - topic ID: $topicId');

    quesDb = await sqlRepo.loadQuestionsByParentId(parentId: topicId);

    print('Phungtd: StudyGameModel- load data - quesDb size: ${quesDb.length}');

    quesDb.forEach((element) {
      if (element.hasChild) {
        // TODO load children questions
      } else {
        questions.add(element);
      }
    });

    // generateGame(questions.sublist(0, 3), StudyType.practice, choicesNum: 4);
    generateGame(questions, StudyType.practice, choicesNum: 4);
    listGames.sort((a, b) => (a.orderIndex < b.orderIndex ? -1 : 1));

    // notifyListeners();
    print('Phungtd: StudyGameModel- load data - listGame size: ${listGames.length}');
    calcProgress();
    onContinue();
  }

  generateGame(List<Question> questions, StudyType type, {int choicesNum}) {
    if (type == StudyType.practice) {
      questions.asMap().forEach((index, question) {
        final gameType = GameType.QUIZ;
        switch (gameType) {
          case GameType.QUIZ:
            final quiz = QuizGameObject.fromQuestion(question);
            if (choicesNum != null) {
              final numOfFakeChoices = choicesNum - quiz.choices.length < questions.length - 1 ? choicesNum - quiz.choices.length : questions.length - 1;
              if (numOfFakeChoices > 0) {
                List<int> availableIndexes = [];
                for (int i = 0; i < questions.length; i++) {
                  if (i != index) {
                    availableIndexes.add(i);
                  }
                }
                availableIndexes.shuffle();
                for (int j = 0; j < numOfFakeChoices; j++) {
                  final choiceToClone = questions[availableIndexes.removeAt(0)].choices[0];
                  final fakeChoice = Choice.cloneWrongChoice(choiceToClone);
                  quiz.choices.add(fakeChoice);
                }
              }
            }
            quiz.choices.shuffle();
            listGames.add(quiz);
            break;
        }
      });
    }
  }

  onAnswer<T>(AnswerType type, T params) async {
    switch (type) {
      case AnswerType.quiz:
        (currentGames as QuizGameObject).onAnswer(params as QuizAnswerParams);
        break;
      default:
        break;
    }
    updateGameProgress();
    calcProgress();
    notifyListeners();
  }

  updateGameProgress<T>([T params]) {
    if (currentGames.status == GameObjectStatus.answered) {
      if (currentGames.questionStatus == QuestionStatus.answeredCorrect) {
        listDone.add(currentGames);
      } else {
        listGames.add(currentGames);
      }
    }
  }

  @override
  calcProgress() {
    List<GameObject> resultList = [];
    resultList.addAll(listGames);
    resultList.addAll(listDone);
    if (currentGames != null &&
        currentGames.status == GameObjectStatus.waiting &&
        currentGames is QuizGameObject) {
      resultList.add(currentGames);
    }
    gameProgress = Progress.calcProgress(resultList);
  }

  @override
  void onContinue({Function callBack}) {
    if (isFinished()) {
      onFinish();
      return;
    }
    currentGames = listGames.removeAt(0);
    if (currentGames.status == GameObjectStatus.answered) {
      if (currentGames.questionStatus == QuestionStatus.answeredIncorrect) {
        currentGames.reset();
      }
    }

    if (callBack != null) {
      callBack();
    }
    notifyListeners();
  }

  @override
  void onFinish() {
    isFinishGame = true;
    notifyListeners();
  }

  bool isFinished() {
    return listDone.isNotEmpty && listGames.isEmpty;
  }

}
