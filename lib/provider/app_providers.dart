import 'package:elearning/provider/study_game_model.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class AppProvider {
  factory AppProvider() {
    if (_instance == null) {
      _instance = AppProvider._getInstance();
    }
    return _instance;
  }

  static AppProvider _instance;
  AppProvider._getInstance();

  // Declare list of model here
  StudyGameModel studyGameModel;

  init() {
    studyGameModel = StudyGameModel();
  }

  List<SingleChildWidget> get provides => [
    ChangeNotifierProvider(create: (_) => studyGameModel),
  ];

}