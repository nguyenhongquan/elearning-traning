import 'package:elearning/model/game/game_object.dart';
import 'package:elearning/model/game/progress.dart';
import 'package:elearning/repository/sql_repository.dart';
import 'package:flutter/material.dart';

enum GameType {
  QUIZ
}

class GameModel extends ChangeNotifier {
  SqfliteRepository sqlRepo = SqfliteRepository();
  List<GameObject> listGames;
  GameObject currentGames;
  Progress gameProgress = Progress();
  bool get isLoading => listGames == null;
  bool isFinishGame = false;
  List<GameObject> listDone = <GameObject>[];

  resetListGame() {
    listGames = [];
    listDone = [];
    currentGames = null;
    gameProgress = Progress();
    isFinishGame = false;
  }
}

class GamePlay {
  void onContinue(){}
  void onFinish(){}
  void calcProgress(){}
}
