import 'package:elearning/model/core/face.dart';
import 'package:flutter/material.dart';

class TextContent extends StatefulWidget {
  final Face face;
  final TextStyle textStyle;

  TextContent({@required this.face, this.textStyle});

  @override
  _TextContentState createState() => _TextContentState();
}

class _TextContentState extends State<TextContent> {
  Face get face => widget.face;
  TextStyle get textStyle => widget.textStyle;

  @override
  Widget build(BuildContext context) {
    return renderText();
  }

  renderText() {
    return Text(
      face.content,
      style: textStyle ?? TextStyle(),
    );
  }
}
