import 'package:elearning/model/core/question.dart';
import 'package:elearning/utils/utils.dart';

class Face {
  String id;
  String content;
  String image;
  String sound;
  String hint;

  Face({this.id, this.content, this.image, this.sound, this.hint});

  Face.fromQuestion(Question question) {
    id = question.id;
    content = question.content;
    image = (question.image != null && question.image.isNotEmpty)
        ? ClientUtils.checkUrl(question.image)
        : null;
    sound = question.sound;
  }
}
