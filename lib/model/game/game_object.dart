import 'package:elearning/model/core/face.dart';
import 'package:elearning/model/core/question.dart';

enum GameObjectStatus { waiting, answered, skip, locked }

class GameObject {
  String id;
  Face question;
  Face explain;   // used in flash game
  Face hint;
  GameObjectStatus status = GameObjectStatus.waiting;
  QuestionStatus questionStatus = QuestionStatus.notAnswerYet;
  double orderIndex;


  GameObject.fromQuestion(Question questionDb) {
    id = questionDb.id;
    question = Face.fromQuestion(questionDb);
    if (questionDb.explain != null && questionDb.explain.isNotEmpty) {
      explain = Face(content: questionDb.explain);
    }
    if (questionDb.hint != null && questionDb.hint.isNotEmpty) {
      hint = Face(content: questionDb.hint);
    }

    orderIndex = questionDb.orderIndex;
  }

  reset() {
    status = GameObjectStatus.waiting;
    questionStatus = QuestionStatus.notAnswerYet;
  }
}